const DeliveryController = require('../controllers/delivery.controller');
const controller = new DeliveryController();

module.exports = (router) => {
    router.route('/deliveries')
        .post(controller.create.bind(controller))
        .get(controller.index.bind(controller));
};
