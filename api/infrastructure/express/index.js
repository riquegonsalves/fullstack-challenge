const express = require('express');
const bodyParser  = require('body-parser');
const cors = require('cors');
const fs = require('fs');
const path = require('path');
const errorHandler = require( './middlewares/error-handler');
const endpointNotFound = require( './middlewares/endpoint-not-found');

const app = express();

app.use(bodyParser.json());
app.use(cors());

const router = express.Router();
try {
    const routesPath = path.resolve(__dirname, './routes');
    const routes = fs.readdirSync(routesPath);
    routes.forEach(route => {
        require(path.resolve(routesPath, route))(router);
    });
} catch(error) {
    console.log('Ocorreu um erro ao carregar as rotas', error);
}

app.use('/', router);
app.use(endpointNotFound);
app.use(errorHandler);


module.exports = app;
