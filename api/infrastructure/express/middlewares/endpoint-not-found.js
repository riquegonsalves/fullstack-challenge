const status = require('http-status');

module.exports = (req, res, next) => {
    res.status(status.NOT_FOUND).json({
        message: 'Endpoint não encontrado'
    });
};
