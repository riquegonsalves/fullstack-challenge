const status = require('http-status');

module.exports = (err, req, res, next) => {
    let statusCode = err.statusCode;
    const payload = { };

    if (['ValidationError', 'MongoError'].includes(err.name)) {
        payload.details = err.datails || null;
        statusCode = status.BAD_REQUEST;
    } else if (!err.statusCode) {
        statusCode = status.INTERNAL_SERVER_ERROR;
    }

    payload.message = err.message;
    res.status(statusCode).json(payload);
};
