const Controller = require('./controller');
const Joi = require('joi');
const DeliveryService = require('../../../business/delivery.service');
const DeliveryValidation = require('../../../utils/validations/delivery.validation'); 

module.exports = class DeliveryController extends Controller {
    constructor() {
        super(new DeliveryService());
    }

    

    async create(req, res, next) {
        try {
            let { error } = Joi.validate(req.body, DeliveryValidation, {
                abortEarly: false
              });
            if(error) throw error;
            const result = await this._service.create(req.body, req.user);
            res.json(result);
        } catch(e) {
            return next(e);
        }
    }
    

    async index(req, res, next) {
        try {
            const results = await this._service.find({});
            const info = await this._service.getListInfo();
            res.json({ results, info });
        } catch(e) {
            return next(new Error(e));
        }
    }
};
