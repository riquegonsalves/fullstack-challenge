module.exports = class Controller {
    constructor(service) {
        this._service = service;
    }

    async create(req, res, next) {
        try {
            const result = await this._service.create(req.body, req.user);
            res.json(result);
        } catch(e) {
            return next(new Error(e));
        }
    }

    async index(req, res, next) {
        try {
            const result = await this._service.find({});
            res.json(result);
        } catch(e) {
            return next(new Error(e));
        }
    }

};