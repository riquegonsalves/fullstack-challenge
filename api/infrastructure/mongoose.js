const mongoose = require('mongoose');
const MONGODB_URI = process.env.MONGODB_URI || 'mongodb://localhost:27017/routEasy';

mongoose.connect(MONGODB_URI, { useNewUrlParser: true });
mongoose.set('debug', true);

module.exports = mongoose;
