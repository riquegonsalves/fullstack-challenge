const mongoose = require("../infrastructure/mongoose");

const DeliverySchema = new mongoose.Schema({
    name: String,
    weight: Number,
    address: {
        street: String,
        number: Number,
        neightborhood: String,
        complement: String,
        city: String,
        state: String,
        country: String,
        location: {
            lat: Number, 
            lon: Number
        }
    }
});



module.exports = mongoose.model('delivery', DeliverySchema);
