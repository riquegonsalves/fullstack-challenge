const Joi = require('joi');

const schema = Joi.object().keys({
    name: Joi.string().required(),
    weight: Joi.number().required(),
    address: Joi.object().keys({
        street: Joi.string().required(),
        number: Joi.number().allow(null).optional(),
        neightborhood: Joi.string().allow('').optional(),
        complement: Joi.string().allow('').optional(),
        city: Joi.string().required(),
        state: Joi.string().required(),
        country: Joi.string().required(),
        location: {
            lat: Joi.number().required(), 
            lon: Joi.number().required()
        }
    }).required()
});

module.exports = schema;
