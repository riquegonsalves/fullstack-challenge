require('dotenv').config();
const supertest = require("supertest");
const faker = require("faker");
const server = require("../../infrastructure/express");
const request = supertest(server);
jest.setTimeout(600000);

describe("DELIVERIES TESTS", () => {
  
  it("CREATE DELIVERY", done => {
    request
    .post(`/deliveries`)
    .send({
      name: faker.name.findName(),
      weight: faker.random.number(300),
      address: {
          street: faker.address.streetName(),
          number: faker.random.number(),
          neightborhood: faker.address.secondaryAddress(),
          complement: 'Complemento',
          city: faker.address.city(),
          state: faker.address.state(),
          country: faker.address.country(),
          location: {
            lat: faker.address.latitude(),
            lon: faker.address.longitude(),
          }
      }
    })
    .end((err, res) => {
        expect(res.statusCode).toBe(200);
        done();
    });
  });

  it("CANT CREATE DELIVERY WITH ERROR", done => {
    request
    .post(`/deliveries`)
    .send({
      name: 'Caio',
      weight: 13,
      address: {
          street: 'Rua',
          number: 12,
          neightborhood: 12,
          complement: 'Complemento',
          city: 'Brasília',
          state: 'DF',
          country: 'BR',
          location: {
              lat: -2031.323, 
              lon: 132.2
          }
      }
    })
    .end((err, res) => {
        expect(res.statusCode).toBe(400);
        done();
    });
  });

  it("GET DELIVERIES", done => {
    request
    .get(`/deliveries`)
    .end((err, res) => {
        expect(res.statusCode).toBe(200);
        done();
    });
  });
  
  afterAll(() => setTimeout(() => process.exit(), 1000))
});
