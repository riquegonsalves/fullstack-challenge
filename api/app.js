if (process.env.NODE_ENV !== 'production') {
  require('dotenv').config();
}

const app = require('./infrastructure/express');
const PORT = process.env.PORT || 4000;

app.listen(PORT, () => {
  console.log(`App running on ${PORT}`);
});
