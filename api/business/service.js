const _ = require('lodash');
module.exports = class Service {
    constructor(model) {
        if (model) {
            this._model = model;
        }
    }

    create(body) {
        return this._model.create(body);
    }

    find(query) {
        return this._model.find(query);
    }
};
