const BaseService = require("./service");
const DeliveryModel = require("../models/delivery.model");

class DeliveryService extends BaseService {
  constructor() {
    super(DeliveryModel);
  }

  getListInfo() {
    return this._model.aggregate([
      { 
        $group: { 
          _id: 'total', 
          total_clients: { $sum: 1 },
          total_weight: { $sum: '$weight' }  
        } 
      }, {
        $project: {
          total_clients: 1,
          total_weight: 1,
          avg_ticket: { $divide: ['$total_weight', '$total_clients'] }
        }
      }
    ]);
  }
}

module.exports = DeliveryService;
