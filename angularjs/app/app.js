'use strict';

// Declare app level module which depends on views, and core components
angular.module('routEasy', [
  'ngRoute',
  'ngMaterial', 
  'ngMessages',
  'routEasy.deliveries',
  'ui-leaflet',
  'md.data.table'
]).
config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');

  $routeProvider.otherwise({redirectTo: '/deliveries'});
}]);
