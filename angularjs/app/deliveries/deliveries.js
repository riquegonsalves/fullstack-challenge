"use strict";

angular
  .module("routEasy.deliveries", ["ngRoute"])

  .config([
    "$routeProvider",
    function($routeProvider) {
      $routeProvider.when("/deliveries", {
        templateUrl: "deliveries/deliveries.html",
        controller: "DeliveriesCtrl"
      });
    }
  ])

  .controller("DeliveriesCtrl", [
    "$scope",
    "$http",
    "$mdToast",
    function($scope, $http, $mdToast) {
      angular.extend($scope, {
        markers: {}
      });

      $scope.deliveries = [];
      $scope.info = {
        avg_ticket: 0,
        total_clients: 0,
        total_weight: 0 
      };

      $scope.apiUrl = 'http://localhost:4000/deliveries';

      function getDeliveries() {
        $http.get($scope.apiUrl).then(function(response) {
          console.log({ data: response.data })
          $scope.deliveries = response.data.results;
          $scope.deliveries.forEach((delivery, index) => {
            $scope.markers['m'+index] = {
              lat: delivery.address.location.lat,
              lng: delivery.address.location.lon,
              message: `${delivery.name} - ${delivery.weight}kg`
            }
          })
          console.log({ deliveries: $scope.deliveries })
          $scope.info = response.data.info[0];
        }, function(err) {
          console.log(err);
        });
      }

      getDeliveries();

      var self = this;
      self.querySearch = querySearch;
      self.submitForm = submitForm;
      self.clearForm = clearForm;
      self.selectedItemChange = selectedItemChange;
      self.searchTextChange = searchTextChange;

      $scope.googleApiUrl = 'https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyB0XGpvvtbFvBUXrC0jtEollayJ-ea-S2I&address=';
      function querySearch(query) {
        return $http.get($scope.googleApiUrl+query).then(function(response) {
          let { results } = response.data;
          return results.map((location) => ({
              value: location.place_id,
              display: location.formatted_address,
              address: {
                location: {
                  lat: location.geometry.location.lat,
                  lon: location.geometry.location.lng
                },
                ... getAddressObject(location.address_components)
              }
              
          }))
        })
      }

      function showToast(message) {
        $mdToast.show(
          $mdToast.simple()
          .textContent(message)
          .position('top')
          .hideDelay(3000)
        )
      }

      function submitForm() {
        $scope.delivery.address = {
          ... $scope.selectedItem.address
        }

        $http.post($scope.apiUrl, $scope.delivery).then(function(response) {
          console.log({ created: response.data })
          showToast('Criado com sucesso!');
          getDeliveries();
          clearForm();
        }, function(err) {
            showToast('Detalhe melhor seu endereço!');
        });
      }

      function clearForm() {
        $scope.delivery = {};
        $scope.selectedItem = undefined;
        $scope.form.$setPristine();
        $scope.form.$setUntouched();
        console.log({ scope: $scope })
      }

      function getAddressObject(address_components) {
        var ShouldBeComponent = {
          number: ["street_number"],
          street: ["street_address", "route"],
          state: [
            "administrative_area_level_1",
          ],
          neightborhood:[
            "administrative_area_level_3",
            "administrative_area_level_4",
            "administrative_area_level_5",
          ],
          city: [
            "administrative_area_level_2",
          ],
          complement: [
            "sublocality",
            "sublocality_level_1",
            "sublocality_level_2",
            "sublocality_level_3",
            "sublocality_level_4"
          ],
          country: ["country"]
        };
  
        var address = {
          street: "",
          number: null,
          neightborhood: "",
          complement: "",
          city: "",
          state: "",
          country: ""
        };
        address_components.forEach(component => {
          for (var shouldBe in ShouldBeComponent) {
            if (ShouldBeComponent[shouldBe].indexOf(component.types[0]) !== -1) {
              if (shouldBe === "country") {
                address[shouldBe] = component.short_name;
              } else {
                address[shouldBe] = component.long_name;
              }
            }
          }
        });
        return address;
      }

      function searchTextChange(text) {
        $log.info("Text changed to " + text);
      }

      function selectedItemChange(item) {
        $log.info("Item changed to " + JSON.stringify(item));
      }

    }
  ]);
